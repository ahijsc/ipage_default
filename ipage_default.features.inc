<?php
/**
 * @file
 * ipage_default.features.inc
 */


/**
 * Implements hook_ctools_plugin_api().
 */

function ipage_default_ctools_plugin_api($module, $api) {
//for exported ctools custom content
  if ($module == 'ctools_custom_content' && $api == 'ctools_content') {
    return array('version' => 1);
  }
//for exported panels pages
 if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
//for exported mini panels
 if ($module == 'panels_mini' && $api == 'panels_default') {
    return array('version' => 1);
  }
}


/**
 * Implements hook_views_api().
 */
function ipage_default_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
